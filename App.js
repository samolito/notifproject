/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { StyleSheet,View, Text, Button } from 'react-native';


const App =()=> {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
         Finally it's working normally
        </Text>
        <Button
          onPress={() => {
            alert('Hello world');
          }}
          title="Press the button"/>
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });

export default App;
